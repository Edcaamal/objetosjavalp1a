/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jgitlab1a;

/**
 *
 * @author edgar
 */
public class JGitLab1A {
        static String[] arreglo = new String[4];
       
    static void llenarArreglo(){
        arreglo[0] = "Hola ";
        arreglo[1] = "Mundo ";
        arreglo[2] = "Programando ";
        arreglo[3] = "en Java";
    }
    static void actualizarArreglo(){
        arreglo[0] = "El ";
        arreglo[1] = "Mundo ";
        arreglo[2] = "es de los que se  ";
        arreglo[3] = "atreven a programar ";
    }
    
    static void imprimirArreglo(){
        // System.out.println(arreglo.length);
        for (int i= 0; i< arreglo.length; i++){
            imprimirCuerpo(arreglo[i]);
        }
    }
    
    static void imprimirCuerpo(String sMensajes){
            System.out.println("  - "+sMensajes);
    }
   
    static void imprimirMensajes(String sMensajes, boolean lineas){
        if (lineas){
            System.out.println(sMensajes);
        }else{
            System.out.print(sMensajes);            
        }
    }
    static void imprimirEntrada(){
        imprimirMensajes("Hello GitLab", false);
        imprimirMensajes("", true);
        imprimirMensajes("Proyecto de Edgar Caamal ", true);
        imprimirMensajes("Prueba de función ", true);
        // imprimirMensajes("", true);
    }    
    static void imprimirseparador(){
        imprimirMensajes("+-----------------------------------------------------------+", true);
    }
    static void imprimirPiedePagina(){
        imprimirseparador();
        imprimirMensajes("(c) EDCD 2020", true);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
//        String[] arreglo;
//        arreglo = new String[4];
        
        llenarArreglo();
        imprimirEntrada();
        imprimirseparador();
        imprimirCuerpo("Hola mundo Java Uacam");
        imprimirCuerpo("Esta materia es la mejor de toda la carrera");
        imprimirCuerpo("En ella se sientan las bases de la programación");
        imprimirCuerpo("Programar es increible");
        imprimirCuerpo("La programación es fuente inspiración");
        imprimirCuerpo("Mas vale codigo en IDE, que 100 volando");
        imprimirCuerpo("Si se puede imaginar se puede programar");
        imprimirseparador();
        imprimirArreglo();
        imprimirseparador();
        actualizarArreglo();
        imprimirArreglo();
        imprimirPiedePagina();        
        
        
    }
    
}
