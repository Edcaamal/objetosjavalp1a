/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javagitlab1;

/**
 *
 * @author edgar
 */
public class JavaGitLab1 {
    static void imprimirMensajes(String sMensajes, boolean lineas){
        if (lineas){
            System.out.println(sMensajes);
        }else{
            System.out.print(sMensajes);            
        }
    }
    static void imprimirEntrada(){
        imprimirMensajes("Hello GitLab", false);
        imprimirMensajes("", true);
        imprimirMensajes("Proyecto de Edgar Caamal ", true);
        imprimirMensajes("Prueba de función ", true);
        imprimirMensajes("", true);
    }    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        imprimirEntrada();
    }
    
}
