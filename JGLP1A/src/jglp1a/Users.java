/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jglp1a;

/**
 *
 * @author edgar
 */
public class Users {
    private int    id;
    private String nombre;
    private String password;
    private int edad;
    private int saldo;

    public Users() {
    }

    public Users(int id, String nombre, String password, int edad, int saldo) {
        this.id = id;
        this.nombre = nombre;
        this.password = password;
        this.edad = edad;
        this.saldo = saldo;
    }



    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }
    
    public void aumentaSaldo(int valor) {
        this.saldo += valor;
    }
    
    public void disminuyeSaldo(int valor) {
        this.saldo -=  valor;
    }

    @Override
    public String toString() {
        return "Users{" + "id=" + id + ", nombre=" + nombre + ", password=" + password + ", edad=" + edad + ", saldo=" + saldo + '}';
    }
    



 
}
