/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jglp1a;

import java.io.Console;
import java.io.IOException;

/**
 *
 * @author edgar
 */
public class Refrigeradores {
    String Marca;
    String Modelo;
    int    anno;
    
    // Static method
    static void myStaticMethod() {
      System.out.println("Método Estatico en una clase");
    }

    // Public method
    public void myPublicMethod() {
      System.out.println("Los métodos publicos se usan para crear una clase ");
      System.out.print(String.format("%-30s", "Nombre"));
      System.out.print(String.format("%-10s", "Edad"));
      System.out.print(String.format("%-5s", "Sexo"));
      System.out.println("");
      System.out.print(String.format("%-30s", "Lesly ......."));
      System.out.print(String.format("%-10s", "22"));
      System.out.print(String.format("%-5s", "F"));
      System.out.println("");
      System.out.print(String.format("%-30s", "Tomas ......."));
      System.out.print(String.format("%-10s", "23"));
      System.out.print(String.format("%-5s", "M"));
      System.out.println("");
      System.out.print(String.format("%-30s", "Juan ......."));
      System.out.print(String.format("%-10s", "23"));
      System.out.print(String.format("%-5s", "M"));
      System.out.println("");
      System.out.print(String.format("%-30s", "Guadalupe ......."));
      System.out.print(String.format("%-10s", "21"));
      System.out.print(String.format("%-5s", "F"));
    }    
    
    static void myClassRefrigerador() {
        Refrigeradores mabe = new Refrigeradores();
       
        
        Refrigeradores hoover = new Refrigeradores();
        
        mabe.Marca  = "Mabe";
        mabe.Modelo = "Semiautomatico";
        mabe.anno   = 2019;
        
        hoover.Marca  = "Hoover";
        hoover.Modelo = "Automatico";
        hoover.anno   = 2020;
         
        System.out.println(mabe.Marca);
        System.out.println(mabe.Modelo);
        System.out.println(mabe.anno);   
        System.out.println("---------------------------");   

        System.out.println(hoover.Marca);
        System.out.println(hoover.Modelo);
        System.out.println(hoover.anno);   
         myStaticMethod();
        hoover.myPublicMethod();
     
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO: code application logic here
        
        Articulos lacteos   = new Articulos("lacteos", 100.00, 50);
        Articulos frutas    = new Articulos("frutas", 50.00, 40);
        Articulos verduras = new Articulos("verduras", 70.00, 80);
        Articulos carnes    = new Articulos("carnes", 200.00, 150);
        
        System.out.println(lacteos.toString());
        lacteos.aumentaExistencia(10);
        System.out.println(lacteos.toString());
        lacteos.disminuyeExistenca(30);
        System.out.println(lacteos.toString());

        
        System.out.println(frutas.toString());
        System.out.println(verduras.toString());
        System.out.println(carnes.toString());
        
        // myClassRefrigerador();
      
        /*
        // Crear y asignar valores a un objeto usuario
        Users jesica = new Users(1, "Jesica", "abc", 23, 1000);
        
        String passlectura = "abc";
        //String passObjeto  = jesica.getPassword();
        
        if(jesica.getPassword().equals(passlectura)){
            System.out.println("----------Acceso Valido-----------");
        } else {
            System.out.println("----------Acceso NO Valido-----------");
        }
            
        
        
        System.out.println("----------Aumenta-----------");
        System.out.println(jesica.toString());
        jesica.aumentaSaldo(150);
        System.out.println(jesica.toString());
        jesica.aumentaSaldo(100);
        System.out.println(jesica.toString());
        jesica.aumentaSaldo(250);
        System.out.println(jesica.toString());
        
  
        System.out.println("----------Disminuye-----------");
        System.out.println(jesica.toString());
        jesica.disminuyeSaldo(150);
        System.out.println(jesica.toString());
        jesica.disminuyeSaldo(100);
        System.out.println(jesica.toString());
        jesica.disminuyeSaldo(250);
        System.out.println(jesica.toString());
        
        
        Users francisco = new Users(2, "Francisco", "123", 27, 5000);
        System.out.println(francisco.toString());
        System.out.println("----------Aumenta-----------");
        francisco.aumentaSaldo(700);
        System.out.println(francisco.toString());
        francisco.aumentaSaldo(1400);
        System.out.println(francisco.toString());
        francisco.aumentaSaldo(2100);
        System.out.println(francisco.toString());
        
        System.out.println("----------Disminuye-----------");
        francisco.disminuyeSaldo(700);
        System.out.println(francisco.toString());
        francisco.disminuyeSaldo(1400);
        System.out.println(francisco.toString());
        francisco.disminuyeSaldo(2100);
        System.out.println(francisco.toString());
        
        jesica.aumentaSaldo(150);
        System.out.println(jesica.toString());
        francisco.disminuyeSaldo(1500);
        System.out.println(francisco.toString());
        

        */
        
        //Users tomas  = new Users();
        
        /*
        int escogerArticulo = 100;
        int ingresarSaldo   = 200;
        System.out.println("---------------------------");   
        System.out.println(jesica.toString());
         
        jesica.aumentaSaldo(ingresarSaldo);
        System.out.println(jesica.toString());
        jesica.disminuyeSaldo(escogerArticulo);
        System.out.println(jesica.toString());
        jesica.disminuyeSaldo(escogerArticulo);
        System.out.println(jesica.toString());
        jesica.disminuyeSaldo(escogerArticulo);
        System.out.println(jesica.toString());
        
        System.out.println("---------------------------");   
       
        //Crear y asignar valorer por los SET a un objeto
        tomas.setId(1);
        tomas.setNombre("Juan");
        tomas.setEdad(22);
        tomas.setSaldo(1500);
        System.out.println(tomas.toString());
        tomas.aumentaSaldo(500);
        System.out.println(tomas.toString());
        tomas.disminuyeSaldo(200);
        System.out.println(tomas.toString());
        tomas.aumentaSaldo(500);
        System.out.println(tomas.toString());
        tomas.aumentaSaldo(500);
        System.out.println(tomas.toString());
*/
        
    }
}
