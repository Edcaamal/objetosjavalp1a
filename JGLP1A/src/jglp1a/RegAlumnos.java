/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jglp1a;


/**
 *
 * @author edgar
 */
public class RegAlumnos {
    
    /**
     * Imprime mensaje, el mensaje es fijo 
     */
    static void mensaje() {
      System.out.println("Los métodos publicos se usan para crear una clase ");
    }    

    /**
     * Un metodo publico 
     */
    public void myPublicMethod() {
      System.out.println("Los métodos publicos se usan para crear una clase ");
    }    
    
    /**
     * @param args Los argumentos de la Linea de comandos 
//     */
    public static void main(String[] args) {
        // TODO code application logic here
       mensaje();
       
        RegAlumnos alumno = new RegAlumnos();
        alumno.myPublicMethod();
    }
    
}
