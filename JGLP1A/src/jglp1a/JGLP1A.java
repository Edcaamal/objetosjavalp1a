/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jglp1a;

/**
 *
 * @author edgar
 */
public class JGLP1A {
    int variable;

    static void imprimirObjeto (Users myUser){
        System.out.println(myUser.getId());
        System.out.println(myUser.getNombre());
        System.out.println(myUser.getEdad());        
    }
    
    static void imprimirMensaje(String sMensaje){
        System.out.println(sMensaje);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
 
      Users estudiante, maestro, administrativo, servicios;
      
      estudiante      = new Users(1,"Edgar", "abc", 30, 1000);
      //estudiante      = new Users(5,"Luis", 50);
      maestro         = new Users(2,"David", "abc", 40, 2000);
      administrativo  = new Users(3,"Juan",  "abc",40,  2500);
      servicios       = new Users(4,"Pedro", "abc",40,  3000);
      
      imprimirObjeto(estudiante);
      imprimirObjeto(maestro);
      imprimirObjeto(administrativo);
      
      imprimirObjeto(servicios);
      servicios.setNombre("Paco");
      imprimirObjeto(servicios);
      
    /*  
        System.out.println(estudiante.getId());
        System.out.println(estudiante.getNombre());
        System.out.println(estudiante.getEdad());

        System.out.println(maestro.getId());
        System.out.println(maestro.getNombre());
        System.out.println(maestro.getEdad());

        System.out.println(administrativo.getId());
        System.out.println(administrativo.getNombre());
        System.out.println(administrativo.getEdad());

        System.out.println(servicios.getId());
        System.out.println(servicios.getNombre());
        System.out.println(servicios.getEdad());
     */   
        /*      for (usuario : user()){
          
      }
*/      
       /*      
       MiClase miObjeto; //Declaramos una variable del tipo de la clase
       miObjeto = new MiClase(); //Aquí ya hemos creado un objeto de MiClase      
       */
             
    }
    
}