/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jglp1a;

/**
 *
 * @author edgar
 */
public class Articulos {
    private String nombre;
    private double precio;
    private int    cantidad;

    public Articulos() {
    }

    public Articulos(String nombre, double precio, int cantidad) {
        this.nombre = nombre;
        this.precio = precio;
        this.cantidad = cantidad;
    }

    public String getNombre() {
        return nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    
      public void aumentaExistencia(int valor) {
        this.cantidad += valor;
    }
    
    public void disminuyeExistenca(int valor) {
        this.cantidad -=  valor;
    }
  
    

    @Override
    public String toString() {
        return "Articulos{" + "nombre=" + nombre + ", precio=" + precio + ", cantidad=" + cantidad + '}';
    }
    
    
    
}
