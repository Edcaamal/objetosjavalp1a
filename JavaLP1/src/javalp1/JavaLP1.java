/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javalp1;

import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;
import java.util.logging.Level;
import sun.rmi.runtime.Log;

/**
 *
 * @author edgar
 */


public class JavaLP1 {
    /**
     * Metodo para imprimir cadenas de texto 
     * TODO Añadir parametro para imprimir un en una linea o en la misma 
     * @param sMensaje
     * @param linea
    */
    public static void texto(String sMensaje, boolean linea){
        /** Validar parametro para imprimir texto con salto de línea o en la misma */
        if (linea) {
           System.out.println(sMensaje);
        } else {
           System.out.print(sMensaje); 
        }
    }

    
    public static void separador (boolean linea){
        if (linea) {
           System.out.println("-------------------------------------------------------");
        } else {
           System.out.print("========================================================"); 
        }
    }   


    public static void encabezado(){
        texto("   Universidad Autónima de Campeche", true);
        texto("       Facultad de Ingenieria", true);
        texto("Ingeniería en Sistemas Computacionales", true);
        texto("     Lenguajes de Programación 1", true);
  }
  

    public static void pie(){
        texto("(c) UACAM-FI", true);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Loger.Log("Test", "Prueba Log", 1);
        String var = "Usuario";
        LOGGER.log(Level.WARNING, var);
        encabezado();
        pie();
      
    }
    
}
