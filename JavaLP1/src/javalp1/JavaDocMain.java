/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javalp1;

import java.util.ResourceBundle;

/**
 * Proyecto para la explicación de generar documentación en Java
 * 
 * @author  Edgar David Caamal Dzulu
 * @author  Universidad Autónoma de Campeche
 * @author  Facultad de Infeniería
 * @author  Ingeniería en Sistemas Computacionales
 * @since   Mayo 2020
 * @version 1.1
 */

public class JavaDocMain {


    /**
     * Primer método de prueba, solo para efectos demostrativos ...
     */
   static void primerMetodo(){
        
    }
   
     /**
     * Nuestro método realiza las funciones de impresón de dos párametros unao tipo cadena, y otro entero
     * @param   cadena1 primer parametro
     * @param   valor1 segundo parametro
     * @return   Regresa la concatención de los parametros
     */ 
    static String segundoMetodo(String cadena1, int valor1 ){
        return cadena1+ valor1;
        
    }
     /**
     * Nuestro tercer método, no tiene parametros 
     * 
     */
    public void tercerMetodo(){
        
    }
   
    /**
     * @param args Programa principal con un arreglo de parametros
     */
    public static void main(String[] args) {
        
        // TODO code application logic here
        
        segundoMetodo("123", 1);
        //JavaDocMain.
        
      
    }
    
}
