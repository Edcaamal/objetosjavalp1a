/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javalp1;

/**
 *
 * @author edgar
 */
public class TestObjetos {
    private static  Usuarios usuario1;
    public static   Usuarios usuario2;
    
    private static void imprimeObjeto(Usuarios myObj){
        System.out.println(myObj.toString());
    }
    
    private static void accion(Usuarios myObj){
        imprimeObjeto(myObj);
    }
    
    private static void Usuario1(){
        accion(usuario1);      
        usuario1.restaSaldo(100);
        accion(usuario1);
        usuario1.restaSaldo(100);
        accion(usuario1);
        usuario1.restaSaldo(100);
        accion(usuario1);
        usuario1.sumaSaldo(200);
        accion(usuario1);
   }    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        usuario1 = new Usuarios("David", 2000);     
        usuario2 = new Usuarios("Juan", 3000);     
        Usuario1();
         System.out.println("-------------------------------------------");
        accion(usuario2);      
        usuario2.restaSaldo(200);
        accion(usuario2);
        usuario2.restaSaldo(300);
        accion(usuario2);
        usuario2.restaSaldo(50);
        accion(usuario2);
        usuario2.sumaSaldo(250);
        accion(usuario2);
        
        
    }
    
}
