/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javalp1;

/**
 *
 * @author edgar
 */
public class Usuarios {
    private String nombre;
    private int    saldo;

    public Usuarios() {
    }

    public Usuarios(String nombre, int saldo) {
        this.nombre = nombre;
        this.saldo = saldo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    @Override
    public String toString() {
        return "usuario{" + "nombre=" + nombre + ", saldo=" + saldo + '}';
    }
    
    public void restaSaldo(int importe){
        this.saldo = saldo - importe;
    }
    
     public void sumaSaldo(int importe){
        this.saldo = saldo + importe;
    }
   
}
