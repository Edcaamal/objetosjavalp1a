/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaobjetoslp1a;

/**
 *
 * @author edgar
 */
public class Materias {
    int    id;
    String Nombre;
    String Facultad;

    public Materias() {
    }

    public Materias(int id, String Nombre, String Facultad) {
        this.id = id;
        this.Nombre = Nombre;
        this.Facultad = Facultad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getFacultad() {
        return Facultad;
    }

    public void setFacultad(String Facultad) {
        this.Facultad = Facultad;
    }

    @Override
    public String toString() {
        return "Materias{" + "id=" + id + ", Nombre=" + Nombre + ", Facultad=" + Facultad + '}';
    }
    
    
    
}
