/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaobjetoslp1a;

import sun.rmi.runtime.Log;

/**
 *
 * @author edgar
 */
public class JavaObjetosLP1A {
   
    
    
    static void saludo(String cadenaSaludo){
        System.out.println("Bueno días");
        System.out.println(cadenaSaludo);
    }
    
    
    public void clase(){
        System.out.println("Lenguaje de Programación 1 ");
    }
    

    static void myPersonas(){
        Persona estudiante        = new Persona();
        estudiante.id             = 1;
        estudiante.nombre         = "Carlos";
        estudiante.edad           = 23;
        estudiante.sexo           = "Masculino";
        estudiante.profesion      = "Estudiante";
        
        System.out.println(estudiante.toString());
        
        Persona maestro            = new Persona();
        maestro.id                 = 2;
        maestro.nombre             = "Edgar";
        maestro.edad               = 48;
        maestro.sexo               = "Masculino";
        maestro.profesion          = "Docente";
        System.out.println(maestro.toString());
        
        Persona administrativo      = new Persona();
        administrativo.id           = 3;
        administrativo.nombre       = "Juan Pablo";
        administrativo.edad         = 40;
        administrativo.sexo         = "Masculino";
        administrativo.profesion    = "Administrativo";
        System.out.println(administrativo.toString());
        
        Persona intendencia         = new Persona(4, "Pedro", 35, "Masculino", "Intendencia");
        Persona prefecto            = new Persona(5, "Pedro", 35, "Masculino", "Prefecto");
        System.out.println(intendencia.toString());
        System.out.println(prefecto.toString());
       
    }
       
    static void myMaterias(){
        Materias lp1 = new Materias(1, "Lenguajes de Programación 1", "FI");
        System.out.println(lp1.toString());

        Materias dam = new Materias(2, "Desarrollo de Aplicaciones Móviles", "FI");
        System.out.println(dam.toString());
        
        Materias daw = new Materias(3, "Desarrollo de Aplicaciones Web", "FI");
        System.out.println(daw.toString());
        
        // Asignaciones a un atributo de la clase
        daw.setId(4);        
        System.out.println(daw.toString());
        daw.id = 5;
        System.out.println(daw.toString());
        
        // Obtener un atributo de la clase
        System.out.println(daw.getNombre());
        System.out.println(daw.id);
        
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //myPersonas();
        //myMaterias();      
        myMaterias();      
    }
    
}
